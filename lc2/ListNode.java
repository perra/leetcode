package com.perra.leetcode.lc2;

public final class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; next = null; }

    @Override
    public String toString() {
	String result = "";
	ListNode n = this;
	do {
	    result += n.val;
	    n = n.next;
	} while (n != null);
	return result;
    }
}
