package com.perra.leetcode.lc2;

import java.util.List;

public final class Solution {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
	ListNode prevNode = null;
	ListNode solution = null;
	int sum = 0;
	int newval = 0;
	int carry = 0;

	while (l1 != null || l2 != null) {
	    // Calculate the sum and remainder.
	    int v1 = l1 != null ? l1.val : 0;
	    int v2 = l2 != null ? l2.val : 0;
	    sum = v1 + v2 + carry;
	    newval = sum % 10;
	    carry = sum > 9 ? 1 : 0;
	    
	    // Update the linked list.
	    ListNode newNode = new ListNode(newval);
	    if (prevNode == null ) {
		solution = newNode;
		prevNode = solution;
	    } else {
		prevNode.next = newNode;
		prevNode = newNode;
	    }
	    if (l1 != null) {
		l1 = l1.next;
	    }
	    if (l2 != null) {
		l2 = l2.next;
	    }
	}
	// Don't forget the carry!
	if (carry != 0) {
	    ListNode newNode = new ListNode(carry);
	    prevNode.next = newNode;
	}
	return solution;
    }

    public static void main(String[] args) {
	ListNode l1 = new ListNode(2);
	ListNode l2 = new ListNode(4);
	ListNode l3 = new ListNode(3);

	ListNode r1 = new ListNode(5);
	ListNode r2 = new ListNode(6);
	ListNode r3 = new ListNode(4);

	l1.next = l2;
	l2.next = l3;

	r1.next = r2;
	r2.next = r3;
	
	System.out.println(addTwoNumbers(l1, r1));
    }
}
